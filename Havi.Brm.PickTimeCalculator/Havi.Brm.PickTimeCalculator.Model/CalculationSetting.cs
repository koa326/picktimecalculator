﻿using Havi.Brm.PickTimeCalculator.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.PickTimeCalculator.Model
{
    public class CalculationSetting
    {
        public int Id { get; set; }
        public int CalculationModelId { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

        public int Dc { get; set; }
        public int KommZone { get; set; }

        public TypeEnum Type { get; set; } //absolute or relative
        public DirectionEnum Direction { get; set; } //forwards / backwards?

        public DateTime SourceDateTime { get; set; } //absolute calculation
        public DayOfWeek SourceWeekDay { get; set; } //relative calculation
        public TimeSpan SourceSlotStart { get; set; } //relative calculation
        public TimeSpan SourceSlotEnd { get; set; } //relative calculation

        public DateTime DestinationDateTime { get; set; } //absolute calculation
        public DayOfWeek DestinationWeekDay { get; set; } //relative calculation
        public int DestinationShift { get; set; }
    }
}
