﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.PickTimeCalculator.Model.Enum
{
    public enum TypeEnum
    {
        Relative = 1,
        Absolute = 2
    }
}
