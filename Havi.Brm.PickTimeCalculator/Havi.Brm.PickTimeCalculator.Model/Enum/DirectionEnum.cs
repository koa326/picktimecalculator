﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.PickTimeCalculator.Model.Enum
{
    public enum DirectionEnum
    {
        Forward = 1,
        Backward = 2
    }
}
