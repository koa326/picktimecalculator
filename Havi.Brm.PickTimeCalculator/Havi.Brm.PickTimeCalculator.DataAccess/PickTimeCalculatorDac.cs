﻿using Havi.Brm.Infrastructure.Data;
using Havi.Brm.Infrastructure.Logger;
using Havi.Brm.PickTimeCalculator.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.PickTimeCalculator.DataAccess
{
    public class PickTimeCalculatorDac : SqlSpAccessBase, IPickTimeCalculatorDac
    {
        private ILogger _logger;

        public PickTimeCalculatorDac(string connectionString, ILogger logger) : base(connectionString, logger)
        {
            _logger = logger;
        }

        public DataTable GetPickTimeCalculatorModel(int id)
        {
            DataTable dt = new DataTable();

            var arg = new SqlParameter[]
                {
                    CreateSqlParameter("@CalculationModelId", id),
                };

            ExecuteDataTable("sp_get_CalculationModel", null, dt);

            return dt;
        }

        public DataTable GetPickTimeCalculatorSettings(int modelId)
        {
            DataTable dt = new DataTable();

            var arg = new SqlParameter[]
                {
                    CreateSqlParameter("@CalculationModelId", modelId),
                };

            ExecuteDataTable("sp_get_CalculationSettings", null, dt);

            return dt;
        }

        public CalculationModel GetCalculationModel(int id)
        {
            var dt = GetPickTimeCalculatorModel(id);

            //TODO: ADD LOGIC

            throw new NotImplementedException();
        }
    }
}
