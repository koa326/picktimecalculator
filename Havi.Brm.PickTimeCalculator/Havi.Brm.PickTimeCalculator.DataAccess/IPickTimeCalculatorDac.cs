﻿using Havi.Brm.PickTimeCalculator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.PickTimeCalculator.DataAccess
{
    public interface IPickTimeCalculatorDac
    {
        CalculationModel GetCalculationModel(int id);
    }
}
