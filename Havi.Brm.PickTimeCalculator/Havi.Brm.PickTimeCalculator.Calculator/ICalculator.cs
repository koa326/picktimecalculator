﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.PickTimeCalculator.Calculator
{
    public interface ICalculator
    {
        /// <summary>
        /// returns the tuple of datetime and pickshift after calculation with calcModelId from constructor
        /// </summary>
        /// <param name="input"></param>
        /// <param name="dc"></param>
        /// <param name="kommZone"></param>
        /// <returns></returns>
        Tuple<DateTime, int> GetPickDateFromLoadTime(DateTime input, int dc, int kommZone);

        /// <summary>
        /// returns the tuple of datetime and pickshift after calculation with given calcModelId
        /// </summary>
        /// <param name="calcModelId"></param>
        /// <param name="input"></param>
        /// <param name="dc"></param>
        /// <param name="kommZone"></param>
        /// <returns></returns>
        Tuple<DateTime, int> GetPickDateFromLoadTime(int calcModelId, DateTime input, int dc, int kommZone);
    }
}
