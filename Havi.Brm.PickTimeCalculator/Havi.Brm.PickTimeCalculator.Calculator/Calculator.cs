﻿using Havi.Brm.Infrastructure.Logger;
using Havi.Brm.PickTimeCalculator.DataAccess;
using Havi.Brm.PickTimeCalculator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.PickTimeCalculator.Calculator
{
    public class Calculator : ICalculator
    {

        private ILogger _logger;
        private IPickTimeCalculatorDac _dac;

        private int _calcModelId;
        private CalculationModel _model;

        /// <summary>
        /// cotr without calcModelId
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="dac"></param>
        public Calculator(ILogger logger, IPickTimeCalculatorDac dac)
        {
            _logger = logger;
            _dac = dac;
        }

        /// <summary>
        /// cotr with calcModelId
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="dac"></param>
        /// <param name="calcModelId"></param>
        public Calculator(ILogger logger, IPickTimeCalculatorDac dac, int calcModelId)
        {
            _logger = logger;
            _dac = dac;

            _calcModelId = calcModelId;
        }

        /// <summary>
        /// inits the model with the given calcModelId
        /// </summary>
        private void Init()
        {
            try
            {
                _model = _dac.GetCalculationModel(_calcModelId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// inits the model with the given calcModelId
        /// </summary>
        /// <param name="calcModelId"></param>
        private void Init(int calcModelId)
        {
            try
            {
                _model = _dac.GetCalculationModel(calcModelId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        /// <summary>
        /// returns the tuple of datetime and pickshift after calculation
        /// </summary>
        /// <param name="input"></param>
        /// <param name="dc"></param>
        /// <param name="kommZone"></param>
        /// <returns></returns>
        public Tuple<DateTime, int> GetPickDateFromLoadTime(DateTime input,int dc, int kommZone)
        {
            //init if _model is empty
            if (null == _model)
                Init();

            try
            {
                //get calcSetting from calcModel
                var calcSet = GetCalcSetting(input, dc, kommZone);

                if (null != calcSet)
                {
                    //calc offset to DesitnationWeekDay
                    var offSet = input.DayOfWeek - (DayOfWeek)calcSet.DestinationWeekDay;

                    //calc datetime
                    var resultDate = input.AddDays(-offSet);

                    //return destination shift and datetime as tuple
                    return new Tuple<DateTime, int>(resultDate, calcSet.DestinationShift);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return null;
        }

        /// <summary>
        /// returns the tuple of datetime and pickshift after calculation
        /// </summary>
        /// <param name="input"></param>
        /// <param name="dc"></param>
        /// <param name="kommZone"></param>
        /// <returns></returns>
        public Tuple<DateTime, int> GetPickDateFromLoadTime(int calcModelId, DateTime input, int dc, int kommZone)
        {
            //init if _model is empty
            if (null == _model)
                Init(calcModelId);

            if (_model.Id != calcModelId)
                Init(calcModelId);

            try
            {
                //get calcSetting from calcModel
                var calcSet = GetCalcSetting(input, dc, kommZone);

                if (null != calcSet)
                {
                    if (calcSet.Type == Model.Enum.TypeEnum.Relative)
                    {
                        if (calcSet.Direction == Model.Enum.DirectionEnum.Backward)
                        {
                            //calc offset to DesitnationWeekDay
                            var offSet = input.DayOfWeek - (DayOfWeek)calcSet.DestinationWeekDay;

                            //calc datetime
                            var resultDate = input.AddDays(-offSet);

                            //return destination shift and datetime as tuple
                            return new Tuple<DateTime, int>(resultDate, calcSet.DestinationShift);
                        }

                        if (calcSet.Direction == Model.Enum.DirectionEnum.Forward)
                        {
                            //calc offset to DesitnationWeekDay
                            var offSet = ((int)(DayOfWeek)calcSet.DestinationWeekDay - (int)input.DayOfWeek + 7) % 7;

                            //calc datetime
                            var resultDate = input.AddDays(offSet);

                            //return destination shift and datetime as tuple
                            return new Tuple<DateTime, int>(resultDate, calcSet.DestinationShift);
                        }
                    }
                    
                    if (calcSet.Type == Model.Enum.TypeEnum.Absolute)
                    {
                        return new Tuple<DateTime, int>(calcSet.DestinationDateTime, calcSet.DestinationShift);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return null;
        }

        /// <summary>
        /// returns the right calcsetting for datetime
        /// </summary>
        /// <param name="input"></param>
        /// <param name="dc"></param>
        /// <param name="kommZone"></param>
        /// <returns></returns>
        private CalculationSetting GetCalcSetting(DateTime input, int dc, int kommZone)
        {
            try
            {
                var settings = _model.Settings.Where(a => a.Dc == dc && a.KommZone == kommZone).OrderBy(o => o.SourceSlotStart);

                if (null != settings)
                {
                    if (settings.Count() > 0)
                    {
                        return settings.FirstOrDefault(w => w.SourceSlotStart <= new TimeSpan(input.Hour, input.Minute, input.Second) && w.SourceSlotEnd >= new TimeSpan(input.Hour, input.Minute, input.Second));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            
            return null;
        }
    }
}
