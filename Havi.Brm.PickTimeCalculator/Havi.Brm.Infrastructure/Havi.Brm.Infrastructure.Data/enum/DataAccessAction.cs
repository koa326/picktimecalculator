﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havi.Brm.Infrastructure.Data
{
    public enum DataAccessAction
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
