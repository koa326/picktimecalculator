﻿## How To Use
#### GET OPERATION:
###### SP:
>		USE [DBNAME]
>		GO
>		SET ANSI_NULLS ON
>		GO
>		SET QUOTED_IDENTIFIER ON
>		GO
>		-- =============================================
>		-- Author:		<Author,,Name>
>		-- Create date: <Create Date,,>
>		-- Description:	<Description,,>
>		-- =============================================
>		Create PROCEDURE [dbo].[SPNAME_GetAll] 
>
>		AS
>		BEGIN
>			-- SET NOCOUNT ON added to prevent extra result sets from
>			-- interfering with SELECT statements.
>			SET NOCOUNT ON;
>
>			-- Insert statements for procedure here
>			SELECT * FROM TABLENAME
>		END

###### C#:
>		public RETURNTYPE GetAll()
>		{
>			DataTable dt = new DataTable();
>			var result = new RETURNTYPE();
>
>			ExecuteDataTable(/* name of stored procedure as string */, null, dt);
>
>			foreach (DataRow row in dt.Rows)
>			{
>				result.Add(new OBJECT() {
>					PROPERTY = DataBase.Turn<TYOE>(row["ROWNAME"]),
>					PROPERTY = DataBase.Turn<>(row["ROWNAME"]),
>					PROPERTY = DataBase.Turn<>(row["ROWNAME"])
>				});
>			}
>
>			return result;
>		}

#### GET WITH PARAMS OPERATION:
###### SP:
>		USE [DBNAME]
>		GO
>		SET ANSI_NULLS ON
>		GO
>		SET QUOTED_IDENTIFIER ON
>		GO
>		-- =============================================
>		-- Author:		<Author,,Name>
>		-- Create date: <Create Date,,>
>		-- Description:	<Description,,>
>		-- =============================================
>		Create PROCEDURE [dbo].[SPNAME_GetBy] 
>		@PARAM1 [PARAMTYPE]
>		@PARAM2 [PARAMTYPE]
>		AS
>		BEGIN
>			-- SET NOCOUNT ON added to prevent extra result sets from
>			-- interfering with SELECT statements.
>			SET NOCOUNT ON;
>
>			-- Insert statements for procedure here
>			SELECT * FROM TABLENAME Where ROW = @INPUTPARAM
>		END

###### C#:
>		public GetBy(TYPE PARAM1, TYPE PARAM2)
>		{
>			DataTable dt = new DataTable();
>
>			var arg = new SqlParameter[]
>			{
>				CreateSqlParameter("@PARAM1", PARAM1),
>				CreateSqlParameter("@PARAM2", PARAM2)
>			};
>
>			var result = /*define result type mostly list or dictonary or what ever */
>
>			ExecuteDataTable(/* name of stored procedure as string */, arg, dt);
>
>			foreach (DataRow row in dt.Rows)
>			{
>				result.Add(new OBJECT() {
>					PROPERTY = DataBase.Turn<TYOE>(row["ROWNAME"]),
>					PROPERTY = DataBase.Turn<>(row["ROWNAME"]),
>					PROPERTY = DataBase.Turn<>(row["ROWNAME"])
>				});
>			}
>
>			return result;
>		}

#### CREATE UPDATE DELETE OPERATION:
###### SP:
>        USE [DBNAME]
>        GO
>        SET ANSI_NULLS ON
>        GO
>        SET QUOTED_IDENTIFIER ON
>        GO
>        -- =============================================
>        -- Author:		<Author,,Name>
>        -- Create date: <Create Date,,>
>        -- Description:	<Description,,>
>        -- =============================================
>        ALTER PROCEDURE [dbo].[SPNAME_CreateUpdateDelete] 
>	        @Action TINYINT,
>	        @PARAM1 nvarchar(512),
>	        @PARAM2 nvarchar(512) = null,
>	        @PARAM3 bit = 0,
>	        @PARAM4 int = null,
>	        @PARAM5 uniqueidentifier = null,
>	        @PARAM6 datetime = null,
>	        @IDNAME TYPEOFID OUTPUT
> 
>         AS
>         BEGIN
> 	        -- SET NOCOUNT ON added to prevent extra result sets from
> 	        -- interfering with SELECT statements.
> 	        SET NOCOUNT ON;
> 
> 	        IF @Action = 1
> 	        BEGIN
> 		        INSERT [dbo].[TABLENAME] (ROWNAME1, ROWNAME2, ROWNAME3, ROWNAME4, ROWNAME5, ROWNAME6)
> 		        VALUES (@PARAM1, @PARAM2, @PARAM3, @PARAM4, @PARAM5, @PARAM6)
> 		        SET @IDNAME = SCOPE_IDENTITY()
> 	        END
> 	        ELSE IF @Action = 2
> 	        BEGIN
> 		        UPDATE [dbo].[TABLENAME] 
> 		        SET [ROWNAME1] = @PARAM1,[ROWNAME2] = @PARAM2
> 		        WHERE [IDNAME] = @IDNAME
> 	        END
> 	        ELSE
> 	        BEGIN
> 		        DELETE [dbo].[TABLENAME] WHERE IDNAME = @IDNAME
> 	        END
>         END

###### C#:
>        public RETURNTYPE Save(INPUTOBJ obj, DataBaseAction action)
>        {
>	        SqlParameter objId = new SqlParameter("@IDNAME", DbType.TYPE);
>	        objId.Direction = ParameterDirection.Input;
>
>	        if (action == DataAccessAction.Insert)
>	        {
>		        objId.Direction = ParameterDirection.Output;
>	        }
>
>	        var arg = new SqlParameter[]
>	        {
>		        CreateSqlParameter("@Action", (int)action),
>		        CreateSqlParameter("@PARAM1", obj.Property1),
>		        CreateSqlParameter("@PARAM2", obj.Property2),
>		        objId
>	        };
>
>	        ExecuteNonQuery("SPNAME", arg);
>
>	        return DataBase.Turn<RETURNTYPE>(objId.Value);
>       }