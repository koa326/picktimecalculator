﻿using System;
using System.Configuration;

namespace Havi.Brm.Infrastructure.Converter
{
    public class Configuration
    {
        public static T GetAppConfig<T>(string key)
        {
            var appSetting = ConfigurationManager.AppSettings[key];

            return null != appSetting ? (T)Convert.ChangeType(appSetting, typeof(T)): default(T);
        }

        public static T GetAppConfig<T>(string key, T defaultValue)
        {
            var appSetting = ConfigurationManager.AppSettings[key];

            return null != appSetting ? (T)Convert.ChangeType(appSetting, typeof(T)) : defaultValue;
        }

        public static string GetAppConfigAsString(string key)
        {
            var appSetting = ConfigurationManager.AppSettings[key];

            return null != appSetting ? appSetting : string.Empty;
        }

        public static string GetAppConfigAsString(string key, string defaultValue)
        {
            var appSetting = ConfigurationManager.AppSettings[key];

            return null != appSetting ? appSetting : defaultValue;
        }

        public static string GetConnectionString(string key)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[key];

            return null != connectionString ? connectionString.ConnectionString : string.Empty;
        }

        public static string GetConnectionString(string key, string defaultValue)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[key];

            return null != connectionString ? connectionString.ConnectionString : defaultValue; 
        }
    }
}
