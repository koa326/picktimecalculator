﻿using System;

namespace Havi.Brm.Infrastructure.Converter
{
    public class Converter
    {
        public static T Turn<T>(object input)
        {
            return (T)Convert.ChangeType(input, typeof(T));
        }
    }
}
