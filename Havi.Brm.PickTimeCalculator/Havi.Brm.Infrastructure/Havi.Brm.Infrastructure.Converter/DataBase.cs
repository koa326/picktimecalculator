﻿using System;

namespace Havi.Brm.Infrastructure.Converter
{
    public class DataBase
    {
        public static T Turn<T>(string rowContent)
        {
            return (T)Convert.ChangeType(rowContent, typeof(T));
        }
    }
}
