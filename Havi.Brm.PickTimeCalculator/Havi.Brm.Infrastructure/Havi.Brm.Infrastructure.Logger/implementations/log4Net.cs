﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using log4net;
using log4net.Config;
using log4net.Core;
using Havi.Brm.Infrastructure.Converter;

namespace Havi.Brm.Infrastructure.Logger.Implementations
{
    public class Log4net : ILogger
    {
        public void Init()
        {
            //read path for logger config from appConfig
            string directory = Configuration.GetAppConfig<string>("LoggerConfigPath",
                AppDomain.CurrentDomain.BaseDirectory);

            //read filename for logger config from appConfig
            var loggerConfig = new FileInfo(directory + Configuration.GetAppConfig<string>("LoggerConfigName", "logger.config"));

            XmlConfigurator.ConfigureAndWatch(loggerConfig);
        }

        public void Info(Exception ex, [CallerMemberName]string callerName = "")
        {
            Info("message {0}, stack: {1}", ex.Message, ex.StackTrace);
        }

        public void Info(string callerName, string msg, params object[] args)
        {
            Info(string.Format(msg, args), callerName);
        }

        public void Info(string msg, [CallerMemberName]string callerName = "")
        {
            if (!LogManager.GetRepository().Configured)
                Init();

            var logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "Log4net");

            logger.Log(new LoggingEvent(new LoggingEventData()
            {
                Level = Level.Info,
                Message = msg,
                TimeStamp = DateTime.Now,
                LoggerName = callerName
            }));
        }

        public void Debug(string msg, [CallerMemberName]string callerName = "")
        {
            if (!LogManager.GetRepository().Configured)
                Init();

            var logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "Log4net");

            logger.Log(new LoggingEvent(new LoggingEventData() { Level = Level.Debug, Message = msg, TimeStamp = DateTime.Now }));
        }

        public void Debug(Exception ex, [CallerMemberName]string callerName = "")
        {
            Debug(callerName, "message {0}, stack: {1}", ex.Message, ex.StackTrace);
        }

        public void Debug(string callerName, string msg, params object[] args)
        {
            Debug(string.Format(msg, args), callerName);
        }

        public void Warn(string msg, [CallerMemberName]string callerName = "")
        {
            if (!LogManager.GetRepository().Configured)
                Init();

            var logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "Log4net");

            logger.Log(new LoggingEvent(new LoggingEventData() { Level = Level.Warn, Message = msg, TimeStamp = DateTime.Now }));
        }

        public void Warn(Exception ex, [CallerMemberName]string callerName = "")
        {
            Warn(callerName, "message {0}, stack: {1}", ex.Message, ex.StackTrace);
        }

        public void Warn(string callerName, string msg, params object[] args)
        {
            Warn(string.Format(msg, args), callerName);
        }

        public void Error(string msg, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0)
        {
            if (!LogManager.GetRepository().Configured)
                Init();

            var logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "Log4net");

            logger.Log(new LoggingEvent(new LoggingEventData()
            {
                Level = Level.Error,
                Message = msg,
                TimeStamp = DateTime.Now,
                LocationInfo = new LocationInfo("", callerName, filePath, lineNumber.ToString())
            }));
        }

        public void Error(Exception ex, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0)
        {
            Error(callerName, "message {0}, stack: {1}, file:{2}, line: {3}", ex.Message, ex.StackTrace, filePath, lineNumber);
        }

        public void Error(string callerName, string msg, params object[] args)
        {
            Error(string.Format(msg, args), callerName);
        }

        public void Fatal(string msg, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0)
        {
            if (!LogManager.GetRepository().Configured)
                Init();

            var logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "Log4net");

            logger.Log(new LoggingEvent(new LoggingEventData()
            {
                Level = Level.Fatal,
                Message = msg,
                TimeStamp = DateTime.Now,
                LocationInfo = new LocationInfo("", callerName, filePath, lineNumber.ToString())
            }));
        }

        public void Fatal(Exception ex, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0)
        {
            Fatal(callerName, "message {0}, stack: {1}, file:{2}, line: {3}", ex.Message, ex.StackTrace, filePath, lineNumber);
        }

        public void Fatal(string callerName, string msg, params object[] args)
        {
            Fatal(string.Format(msg, args));
        }
    }
}
