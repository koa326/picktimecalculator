﻿using System;
using System.Runtime.CompilerServices;

namespace Havi.Brm.Infrastructure.Logger
{
    public interface ILogger
    {
        void Init();

        void Info(string msg, [CallerMemberName]string callerName = "");
        void Info(Exception ex, [CallerMemberName]string callerName = "");
        void Info(string callerName, string msg, params object[] args);

        void Debug(string mgs, [CallerMemberName]string callerName = "");
        void Debug(Exception ex, [CallerMemberName]string callerName = "");
        void Debug(string callerName, string mgs, params object[] args);

        void Warn(string mgs, [CallerMemberName]string callerName = "");
        void Warn(Exception ex, [CallerMemberName]string callerName = "");
        void Warn(string callerName, string mgs, params object[] args);

        void Error(string mgs, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0);
        void Error(Exception ex, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0);
        void Error(string callerName, string mgs, params object[] args);

        void Fatal(string mgs, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0);
        void Fatal(Exception ex, [CallerMemberName]string callerName = "", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNumber = 0);
        void Fatal(string callerName, string mgs, params object[] args);
    }
}
