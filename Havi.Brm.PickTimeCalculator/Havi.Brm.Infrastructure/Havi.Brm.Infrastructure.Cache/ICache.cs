﻿using System;
using System.Xml;

namespace Havi.Brm.Infrastructure.Cache
{
    public interface ICache
    {
        void Add(string key, object value);
        void Add(string key, object value, DateTime expiration);
        void Add(string key, object value, TimeSpan expiration);

        void Remove(string key);

        bool Contains(string key);
        int Count();
        void Flush();

        T GetData<T>(string key);
        object GetData(string key);

        object this[string key] { get; }
        string GetDataAsString(string key);
        string GetDataAsJson(string key);
        XmlDocument GetDataAsXml(string key);
    }
}
