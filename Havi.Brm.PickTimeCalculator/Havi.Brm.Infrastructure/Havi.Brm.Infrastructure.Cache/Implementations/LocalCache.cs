﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Havi.Brm.Infrastructure.Logger;
using Newtonsoft.Json;
using System.Xml;

namespace Havi.Brm.Infrastructure.Cache.Implementations
{
    public class LocalCache : ICache
    {
        private readonly ConcurrentDictionary<string, object> _cache;
        private readonly ConcurrentDictionary<string, DateTime> _cacheDates;
        private CancellationTokenSource _tokenSource = new CancellationTokenSource();
        private ILogger _logger;

        public LocalCache()
        {
            _cache = new ConcurrentDictionary<string, object>();
            _cacheDates = new ConcurrentDictionary<string, DateTime>();

            Task.Run(async () => await CleanupAsync());
        }

        public LocalCache(ILogger logger)
        {
            _cache = new ConcurrentDictionary<string, object>();
            _cacheDates = new ConcurrentDictionary<string, DateTime>();

            Task.Run(async () => await CleanupAsync());

            _logger = logger;
        }

        public object this[string key]
        {
            get
            {
                return _cache[key];
            }
        }

        public void Add(string key, object value)
        {
            Add(key, value, DateTime.Now.AddHours(1));
        }

        public void Add(string key, object value, TimeSpan expiration)
        {
            Add(key, value, DateTime.Now.AddTicks(expiration.Ticks));
        }

        public void Add(string key, object value, DateTime expiration)
        {
            _cache.AddOrUpdate(key, value, (keyValue, oldValue) => value);
            _cacheDates.AddOrUpdate(key, expiration, (keyValue, oldValue) => expiration);
        }

        public bool Contains(string key)
        {
            return _cache.ContainsKey(key);
        }

        public int Count()
        {
            return _cache.Count;
        }

        public void Flush()
        {
            _cache.Clear();
            _cacheDates.Clear();
        }

        public object GetData(string key)
        {
            if (Contains(key))
            {
                return _cache[key];
            }

            return null;
        }

        public T GetData<T>(string key)
        {
            if (Contains(key))
            {
                return (T)_cache[key];
            }

            return default(T);
        }

        public string GetDataAsJson(string key)
        {
            if (Contains(key))
            {
                return JsonConvert.SerializeObject(_cache[key]);
            }

            return string.Empty;
        }

        public XmlDocument GetDataAsXml(string key)
        {
            if (Contains(key))
            {
                return JsonConvert.DeserializeXmlNode(GetDataAsJson(key));
            }

            return null;
        }

        public string GetDataAsString(string key)
        {
            if (Contains(key))
            {
                return _cache[key].ToString();
            }

            return string.Empty;
        }

        public void Remove(string key)
        {
            throw new NotImplementedException();
        }

        public async Task CleanupAsync()
        {
            while(!_tokenSource.Token.IsCancellationRequested)
            {
                foreach (var key in _cacheDates.Keys)
                {
                    if (_cacheDates[key] < DateTime.Now)
                    {
                        Remove(key);
                    }
                }

                await Task.Delay(TimeSpan.FromSeconds(5), _tokenSource.Token);
            }
        }
    }
}
