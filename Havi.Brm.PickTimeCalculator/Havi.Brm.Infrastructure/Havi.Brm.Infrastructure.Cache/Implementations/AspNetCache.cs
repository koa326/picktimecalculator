﻿using Havi.Brm.Infrastructure.Logger;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Web;
using System.Web.Caching;
using System.Xml;

namespace Havi.Brm.Infrastructure.Cache.Implementations
{
    public class AspNetCache : ICache
    {
        private ILogger _logger;

        public AspNetCache()
        {

        }

        public AspNetCache(ILogger logger)
        {
            _logger = logger;
        }

        public object this[string key]
        {
            get
            {
                return GetData(key);
            }
        }

        public void Add(string key, object value)
        {
            Add(key, value, DateTime.Now.AddHours(1));
        }

        public void Add(string key, object value, TimeSpan expiration)
        {
            Add(key, value, DateTime.Now.AddTicks(expiration.Ticks));
        }

        public void Add(string key, object value, DateTime expiration)
        {
            if (null != value)
            {
                HttpRuntime.Cache.Add(key, value, null, expiration, System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }
        }

        public bool Contains(string key)
        {
            return HttpRuntime.Cache.Get(key) != null;
        }

        public int Count()
        {
            return HttpRuntime.Cache.Count;
        }

        public void Flush()
        {
            IDictionaryEnumerator enumerator = HttpRuntime.Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                HttpRuntime.Cache.Remove((string)enumerator.Key);
            }
        }

        public object GetData(string key)
        {
            if (Contains(key))
            {
                return HttpRuntime.Cache.Get(key);
            }

            return null;
        }

        public T GetData<T>(string key)
        {
            if (Contains(key))
            {
                return (T)GetData(key);
            }

            return default(T);
        }

        public string GetDataAsJson(string key)
        {
            if (Contains(key))
            {
                return JsonConvert.SerializeObject(HttpRuntime.Cache.Get(key));
            }

            return string.Empty;
        }

        public XmlDocument GetDataAsXml(string key)
        {
            if (Contains(key))
            {
                return JsonConvert.DeserializeXmlNode(GetDataAsJson(key));
            }

            return null;
        }

        public string GetDataAsString(string key)
        {
            if (Contains(key))
            {
                return HttpRuntime.Cache.Get(key).ToString();
            }

            return string.Empty;
        }

        public void Remove(string key)
        {
            if (Contains(key))
            {
                HttpRuntime.Cache.Remove(key);
            }
        }
    }
}
